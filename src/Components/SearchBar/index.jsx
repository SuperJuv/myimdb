import React, {Component} from 'react';
import './style.css'

class SearchBar extends Component {

    render() {
        return (
            <div>
                <input value={this.props.value} className="SearchInput" type="text" onChange={this.props.onSearch}/>
            </div>
        );
    }
}

export default SearchBar;