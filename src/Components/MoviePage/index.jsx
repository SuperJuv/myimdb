import React, {Component} from 'react';
import axios from 'axios';

import './style.css'

const BASE_API = 'http://www.omdbapi.com/?apikey=6d853484&i='


class MoviePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movieDetails : {}
        }
    }

    componentDidMount() {
        const {id} = this.props.match.params
        if(id) {
            axios.get(BASE_API+id)
                .then(data => {
                    const { imdbID,type,DVD,BoxOffice,Response,...res} = data.data
                    this.setState({
                        movieDetails: res
                    })
                })
                .catch(error => console.log('pickels=> error',error))
        }
    }

    render() {
        const {movieDetails} = this.state
        return (
            <div className="page">
                <ul className="infoList">
                    {Object.keys(movieDetails).map( (item, index) => {
                        return (
                            <li className="row" key={index}>
                                <div className="prop">{item}</div>
                                <div className="info">{
                                    typeof movieDetails[item] === 'string'
                                    ? (movieDetails[item])
                                        : (movieDetails[item]).map( (subitem, index) => {
                                            return(
                                                <div className="row" key={index}>
                                                    <div className="subProp">{subitem.Source}</div>
                                                    <div className="info">{subitem.Value}</div>
                                                </div>
                                            )
                                        })
                                }</div>
                            </li>
                        )
                    })}
                </ul>
            </div>
        );
    }
}

export default MoviePage;