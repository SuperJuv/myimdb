import React, {Component} from 'react';
import './style.css'
import {Link} from "react-router-dom"

class MovieItem extends Component {
    render() {
        const {item} = this.props
        return (
            <li className="movieListItem">
                <Link to={`/movies/${item.imdbID}`}>
                    <img src={item.Poster} alt=""/>
                    <span>{item.Title} <b>({item.Year})</b></span>
                </Link>
            </li>
        );
    }
}

export default MovieItem;