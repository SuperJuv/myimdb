import React, {Component} from 'react';
import {BrowserRouter, Redirect} from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import './style.css'

import SearchPage from "../SearchPage"
import MoviePage from "../MoviePage"

class Layout extends Component {
    render() {
        return (
            <BrowserRouter>
                <header>IMDB-like application</header>
                <div className="content">
                    <Switch>
                        <Route exact path='/' component={SearchPage}/>
                        <Route exact path='/movies/:id' component={MoviePage}/>
                        <Redirect from="/movies/" to='/' />
                        <Route render={() => <Redirect to='/' />} />
                    </Switch>
                </div>
                <footer>* data taken from http://www.omdbapi.com/</footer>
            </BrowserRouter>
        );
    }
}

export default Layout;