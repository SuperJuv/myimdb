import React, {Component} from 'react';
import SearchBar from "../SearchBar"
import MovieList from "../MovieList"
import axios from 'axios';
import * as qs from 'query-string'

const BASE_API = 'http://www.omdbapi.com/?apikey=6d853484&type=movie&s='


function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

class SearchPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchValue: '',
            searchItems: []
        }


        this.onSearch = this.onSearch.bind(this)
        this.featchData = debounce(this.featchData.bind(this),300)
    }

    componentDidMount() {
        const query = qs.parse(this.props.location.search, { ignoreQueryPrefix: true })
        if(query.q) {
            this.onSearch(query.q)
        }
    }


    onSearch(value) {
        this.setState({searchValue: value})
        this.featchData(value)
    }

    featchData(value) {
        axios.get(BASE_API+value)
            .then(data => {
                if(data.data.Response === "True") {
                    this.setState({
                        searchItems:data.data.Search
                    })
                } else {
                    this.setState({
                        searchItems: [{Title:data.data.Error}]
                    })
                }
            }).catch(error => console.log('pickels=> error',error))
    }

    render() {
        return (
            <div className="page">
                <SearchBar
                    value={this.state.searchValue}
                    onSearch={(e) => this.onSearch(e.target.value)}
                />
                <MovieList searchItems={this.state.searchItems}/>
            </div>
        );
    }
}

export default SearchPage;