import React, {Component} from 'react';
import MovieListItem from "../MovieListItem"
import './style.css'

class MovieList extends Component {

    render() {
        const {searchItems} = this.props
        return (
            <ul className="movieList">
                {searchItems.map((item,index) =>  <MovieListItem key={index} item={item}/>)}
            </ul>
        );
    }
}

export default MovieList;